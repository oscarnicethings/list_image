# -*- coding: utf-8 -*-
########################
# Created on 27/8/2014
#
# @author: openerp
########################

{
    'name': "Enable image-display in list view",
    'description': "Makes binary image fields display in list and tree views",
    'category': 'Base',
    'depends': ['web'],
    'js': ['static/src/js/view_list.js'],
}